from time import ctime


class LoggingMiddleware:
    PATH = 'logging.log'

    def __init__(self, get_response):
        self.__get_response = get_response

    def __call__(self, request):
        response = self.__get_response(request)
        with open(self.PATH, "w", encoding="utf-8") as f:

            data = {'datetime': ctime(), 'HTTP-method': request.method,
                    'PATH INFO': request.META['PATH_INFO'], 'Server Protocol': request.META['SERVER_PROTOCOL']}

            print(data, file=f)
        return response
