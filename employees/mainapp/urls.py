from django.urls import path
from django.urls import include, path
from rest_framework import routers
from mainapp import views

from . import views
router = routers.DefaultRouter()
router.register(r'personal_data', views.PersonalDataList, basename='personal-data-list')

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('employees/', views.EmloyeesList.as_view(), name='employees-list'),


]


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
