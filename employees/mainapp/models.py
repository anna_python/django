from django.db import models
from django.core.exceptions import ValidationError
import re
from  .validators import phone_number_validator
from companyapp.models import *

# Create your models here.
# from employees.companyapp.models import *
NULL_BLANK = {'null': True, 'blank': True}


class Employee(DateModel):
    name = models.CharField(max_length=20)
    surname = models.CharField(max_length=20)
    job_position = models.CharField(max_length=40, verbose_name='job position')
    is_manager = models.BooleanField(default=False, verbose_name='is manager')
    is_admin = models.BooleanField(default=False, verbose_name='is admin')
    company = models.ForeignKey(Company, related_name='employees', on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=30, verbose_name='phone number', validators=[phone_number_validator])

    class Meta:
        indexes = [
            models.Index(fields=['name', 'surname']),
        ]


    def __str__(self):
        return f'{self.name} {self.surname}'


class PersonalData(DateModel):
    employee = models.OneToOneField(Employee, related_name="data", on_delete=models.CASCADE, primary_key=True)
    date_of_birth = models.DateField(verbose_name='date of birth')
    home_address = models.CharField(max_length=150, verbose_name="address")
    salary = models.DecimalField(decimal_places=2, max_digits=8)

    def __str__(self):
        return self.employee.name

class Tmp(models.Model):
    name = models.CharField(max_length=20)