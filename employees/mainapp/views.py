from django.shortcuts import HttpResponse
from django.views.generic import *
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from mainapp.serializers import *
from rest_framework import viewsets
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

# Create your views here.
class BaseView(View):
    def get(self, request):
        return HttpResponse('Добро пожаловать!')


class PersonalDataList(viewsets.ModelViewSet):
   serializer_class = PersonalDataSerializer
   queryset = PersonalData.objects.all()


class EmloyeesList(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request):
        employees = Employee.objects.all()
        serializer = EmployeeSerializer(instance=employees, many=True)
        return Response(serializer.data)


