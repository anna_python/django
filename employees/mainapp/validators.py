import re
from django.core.exceptions import ValidationError


def phone_number_validator(phone_number):
    regex = re.compile('(?:80(?:29|25|33|44)|[+]375(?:29|33|44|25|17))\d{7}')
    number = re.match(regex, phone_number)
    if not number or number.group(0) != phone_number:
        raise ValidationError("Wrong number!")
