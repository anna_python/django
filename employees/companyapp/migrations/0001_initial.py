# Generated by Django 3.2.9 on 2021-12-05 19:37

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bank',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('web_site', models.URLField(verbose_name='website')),
                ('email', models.EmailField(max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('web_site', models.URLField(verbose_name='website')),
                ('email', models.EmailField(max_length=254)),
                ('logo', models.ImageField(upload_to='media/logos')),
                ('post_index', models.IntegerField(verbose_name='post index')),
            ],
        ),
    ]
