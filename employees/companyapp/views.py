from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.generics import *
from companyapp.serializers import *


class CompaniesList(ListCreateAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    # permission_classes = [IsAuthenticated]

@api_view(http_method_names=['GET'])
def show_bank_list(request):
    banks = Bank.objects.all()
    serializer = BankSerializer(instance=banks, many=True)
    return Response(serializer.data)
